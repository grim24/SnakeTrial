/**
Snake
by Landon Grim
Classic Nokia Phone Game Remake

{req} browser with html5 and javascript capabilities
*/


function playSnake()
{

    //hide new game button
	document.getElementById('newGame').style.visibility='hidden';
	
    var canvas = document.getElementById('gameplay');
    gameCanvas = canvas.getContext('2d');
	
	 

    canvas.width = 404;
    canvas.height = 424;

//attach canvas to body (html5)
    var body = document.getElementsByTagName('body')[0];
	
    score = 0,
    level = 0,
    direction = 0,
    snake = new Array(5);//change number for snake start size
    active = true,
    speed = 170; //change this if too slow(lower the faster)
    

    //initialize grid
   var gameArray = new Array(40);
    for (var i = 0; i < gameArray.length; i++) {
        gameArray[i] = new Array(40);
    }

    
    gameArray = initialSnake(gameArray);
    gameArray = randomFood(gameArray);

    drawGame();

    window.addEventListener('keydown', function(e) {
        if (e.keyCode === 38 && direction !== 3) {
            direction = 2; // Up
        } else if (e.keyCode === 40 && direction !== 2) {
            direction = 3; // Down
        } else if (e.keyCode === 37 && direction !== 0) {
            direction = 1; // Left
        } else if (e.keyCode === 39 && direction !== 1) {
            direction = 0; // Right
        }
    });

    function drawGame()
    {
        // Clear the canvas
        gameCanvas.clearRect(0, 0, canvas.width, canvas.height);

        // Traverse all the body pieces of the snake, starting from the last one
        for (var i = snake.length - 1; i >= 0; i--) {

            //direction
            if (i === 0) {
                switch(direction) {
                    case 0: // Right
                        snake[0] = { x: snake[0].x + 1, y: snake[0].y }
                        break;
                    case 1: // Left
                        snake[0] = { x: snake[0].x - 1, y: snake[0].y }
                        break;
                    case 2: // Up
                        snake[0] = { x: snake[0].x, y: snake[0].y - 1 }
                        break;
                    case 3: // Down
                        snake[0] = { x: snake[0].x, y: snake[0].y + 1 }
                        break;
                }

                // Check that it's not ob
                if (snake[0].x < 0 ||
                    snake[0].x >= 40 ||
                    snake[0].y < 0 ||
                    snake[0].y >= 40) {
                    showGameOver();
                    return;
                }

                //detect if hit food.  if hit food, add to snake
                if (gameArray[snake[0].x][snake[0].y] === 1) {
                    score += 10;
                    gameArray = randomFood(gameArray);

                    // Add a new body piece to the array
                    snake.push({ x: snake[snake.length - 1].x, y: snake[snake.length - 1].y });
                    gameArray[snake[snake.length - 1].x][snake[snake.length - 1].y] = 2;

                   //increase level every 50 points
                    if ((score % 50) == 0) {
                        level += 1;
                    }
                
                     //end game if snake hits itself
				
                    } else if (gameArray[snake[0].x][snake[0].y] === 2) {
                    showGameOver();
                    return;
                    }

                gameArray[snake[0].x][snake[0].y] = 2;
            } else {
                // last position and shift snake
                if (i === (snake.length - 1)) {
                    gameArray[snake[i].x][snake[i].y] = null;
                }

                snake[i] = { x: snake[i - 1].x, y: snake[i - 1].y };
                gameArray[snake[i].x][snake[i].y] = 2;
            }
        }

        // Draw the border as well as the score
        scoreAndBorder();

        // if 2d array value = 1, fill in blue for food
        // if 2d array value = 2, fill in red for snake
        for (var x = 0; x < gameArray.length; x++) {
            for (var y = 0; y < gameArray[0].length; y++) {
                if (gameArray[x][y] === 1) {
                    gameCanvas.fillStyle = 'blue';
                    gameCanvas.fillRect(x * 10, y * 10 + 20, 9, 9);
                } else if (gameArray[x][y] === 2) {
			
					gameCanvas.fillStyle='red';
                    gameCanvas.fillRect(x * 10, y * 10 + 20, 9, 9);
                }
            }
        }
        
        if (active) {
            setTimeout(drawGame, speed - (level * 10));
        }
    }
	

	/**
        Draws main board and score
    */
    function scoreAndBorder()
    {
        gameCanvas.lineWidth = 2; // border
        gameCanvas.strokeStyle = 'yellow'; 
		//leave 20 pix at top
        gameCanvas.strokeRect(2, 20, canvas.width - 4, canvas.height - 24);

        gameCanvas.fillStyle = 'red';
        gameCanvas.font = '12px sans-serif';
        gameCanvas.fillText('Score: ' + score + ' - Level: ' + level, 2, 12);
    }

    /**
        Puts random peice of food on game array
    */
    function randomFood(gameArray)
    {
        // Generate a random position for the rows and the columns.
        var rndX = Math.round(Math.random() * 39),
            rndY = Math.round(Math.random() * 39);
        
        //don't generate food already in snake body 
        while (gameArray[rndX][rndY] === 2) {
            rndX = Math.round(Math.random() * 39);
            rndY = Math.round(Math.random() * 39);
        }
        
        gameArray[rndX][rndY] = 1;

        return gameArray;
    }

    /**
        Generate snake at start of game
    */
    function initialSnake(gameArray)
    {
        // Generate a random position for the row and the column of the head.
        var rndX = Math.round(Math.random() * 39),
            rndY = Math.round(Math.random() * 39);

        //make sure not ob
        while ((rndX - snake.length) < 0) {
            rndX = Math.round(Math.random() * 39);
        }
        
        for (var i = 0; i < snake.length; i++) {
            snake[i] = { x: rndX - i, y: rndY };
            gameArray[rndX - i][rndY] = 2;
        }

		//snake should be ready to go
        return gameArray;
		
    }


/**
    clears screen, displays screen and 
*/
    function showGameOver()
    {
		
        active = false;

        var endmessage = "You suck you JACKASS"
        // Clear screen
        gameCanvas.clearRect(0, 0, canvas.width, canvas.height);
        gameCanvas.fillStyle = 'red';
        gameCanvas.font = '16px sans-serif'; 
        gameCanvas.fillText(endmessage, ((canvas.width / 2) - (gameCanvas.measureText(endmessage).width / 2)), 50);
        gameCanvas.font = '12px sans-serif';
        gameCanvas.fillText('Your Score Was: ' + score, ((canvas.width / 2) - (gameCanvas.measureText('Your Score Was: ' + score).width / 2)), 70);
        
        //show new game button
		document.getElementById('newGame').style.visibility='visible';
		
    }
	
	
};




function NewGame(){
playSnake();
}
